'use-strict';

angular.module('ahlowebinterface')

.directive('productSearch', [
    function () {
        return {
            restrict: "EA",
            scope: {
                selectedProducts: '=',
                quotationId: '=',
            },
            templateUrl: "views/product-search.html",
            controller: "ProductSearchController",
            link: function (scope) {},
        };
}]);
