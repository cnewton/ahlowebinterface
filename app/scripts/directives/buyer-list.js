'use-strict';

angular.module('ahlowebinterface')

.directive('buyerList', [
    function () {
        return {
            restrict: "EA",
            scope: {
                selectedBuyer: "=",
                buyerSelected: "=",
            },
            templateUrl: "views/buyer-list.html",
            controller: "BuyerListController",
            link: function (scope) {},
        };
}]);
