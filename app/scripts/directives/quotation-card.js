/**
 * Created by Cameron on 2016-08-02.
 */

'use strict';

angular.module('ahlowebinterface')

  .directive('quotationCard', [
    'QuotationService',
    '$state',
    function (QuotationService, $state) {
      return {
        restrict: "E",
        scope: {
          quotation: '=',
          modifiable: '=',
          expanded: '='
        },
        templateUrl: "views/quotation-card.html",
        link: function (scope) {

          //Watch for changes in the quotation and populate it when changed.
          scope.$watch('quotation', function () {
            if (scope.expanded) {
              populateFunction();
            }
          });

          var populateFunction = function () {
            //If Products are not already present on this quotation.
            if (!scope.quotation.products) {
              //Load quotation fully and extract products.
              QuotationService.find(scope.quotation.id).then(
                function successCallback(response) {
                  scope.quotation.products = response.data.products;
                }
              );
            }
          };

          //Toggle expansion and populate newly expanded cards.
          scope.expand = function () {
            if (scope.expanded) {
              scope.expanded = false;
            }
            else {
              scope.expanded = true;
              populateFunction();
            }
          };

          //Route to the build page.
          scope.modifyQuotation = function () {
            $state.go('page.quotationbuild', {
              quoteId: scope.quotation.id
            });
          };
        }
      };
    }]);
