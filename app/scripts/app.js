'use-strict';

angular.module("ahlowebinterface", [
    'ui.router',
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.selection',
    'ui.grid.autoResize',
    'ui.grid.resizeColumns',
    'ngAnimate',
    'ajoslin.promise-tracker',
    'cgBusy',
    'ui.bootstrap',
    'bootstrapLightbox',
    'base64',
    'angularFileUpload',
])

.config(['$stateProvider', '$urlRouterProvider', 'PERMISSION_LEVEL', function ($stateProvider, $urlRouterProvider, PERMISSION_LEVEL) {

    $urlRouterProvider.otherwise('/login');

    $stateProvider

        .state('login', {
            url: '/login',
            templateUrl: 'login.html',
            controller: 'LoginController',
            access: {
                loggedIn: false
            }
        })
        .state('page', {
            abstract: true,
            templateUrl: 'views/root.html'
        })
        .state('page.home', {
            url: '/home',
            templateUrl: 'views/home.html',
            access: {
                loggedIn: true,
            }
        })
        .state('page.quotationsreview', {
            url: '/quotations/review',
            templateUrl: 'views/quotations-review.html',
            controller: "QuotationReviewController",
            access: {
                loggedIn: true,
                permissions: [PERMISSION_LEVEL.USER],
            }
        })
        .state('page.quotationsmanage', {
            url: '/quotations/manage',
            templateUrl: 'views/quotations-manage.html',
            controller: "QuotationManageController",
            access: {
                loggedIn: true,
                permissions: [PERMISSION_LEVEL.USER],
            }
        })
        .state('page.quotationshistory', {
            url: '/quotations/history',
            templateUrl: 'views/quotations-history.html',
            controller: "QuotationHistoryController",
            access: {
                loggedIn: true,
                permissions: [PERMISSION_LEVEL.USER],
            }
        })
        .state('page.quotationbuild', {
            url: '/quotation/{quoteId:int}/build',
            templateUrl: 'views/quotation-build.html',
            controller: "QuotationBuildController",
            access: {
                loggedIn: true,
                permissions: [PERMISSION_LEVEL.USER],
            }
        })
        .state('page.quotationview', {
            url: '/quotation/{quoteId:int}/view',
            templateUrl: 'views/quotation-view.html',
            controller: "QuotationViewController",
            access: {
                loggedIn: true,
                permissions: [PERMISSION_LEVEL.USER],
            }
        })
        .state('page.recordview', {
            url: '/record/{quoteId:int}/view',
            templateUrl: 'views/quotation-view.html',
            controller: "QuotationRecordViewController",
            access: {
                loggedIn: true,
                permissions: [PERMISSION_LEVEL.USER],
            },
        })
        .state('page.accounts', {
            url: '/accounts',
            templateUrl: 'views/accounts.html',
            controller: "AccountsController",
            access: {
                loggedIn: true,
                permissions: [PERMISSION_LEVEL.USER],
            },
        })
        .state('page.productlist', {
            url: '/products',
            templateUrl: 'views/product-list.html',
            controller: "ProductListController",
            access: {
                loggedIn: true,
                permissions: [PERMISSION_LEVEL.USER],
            }
        });
}]);
