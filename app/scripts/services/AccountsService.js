'use-strict';

angular.module("ahlowebinterface")

.factory("AccountsService", ["$http", "BACKEND", function AccountsService($http, BACKEND) {
    return {
        all: function () {
            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/user"
            });
        },
        find: function (userId) {
            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/user/" + userId,
            });
        },
        account: function () {
            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/account"
            });
        },
        update: function (user) {
            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/user",
                data: user
            });
        },

        create: function (user) {
            return $http({
                method: 'post',
                url: BACKEND.LOCATION + "/user",
                data: user
            });
        },

        delete: function (userId) {
            return $http({
                method: 'delete',
                url: BACKEND.LOCATION + "/user/" + userId,
            });
        },

        userChangePassword: function (userId, oldPassword, newPassword) {
            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/user/" + userId + "/password",
                data: [oldPassword, newPassword]
            });
        },

        adminChangePassword: function (userId, newPassword) {
            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/user/" + userId + "/password/force",
                data: '"' + newPassword + '"'
            });
        },
    };
}]);
