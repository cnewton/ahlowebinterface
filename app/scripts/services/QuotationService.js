'use-strict';

angular.module("ahlowebinterface")

.factory("QuotationService", ["$http", "BACKEND", function QuotationService($http, BACKEND) {
    return {
        all: function () {

            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/quotation"
            });
        },
        allReview: function () {

            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/quotation/review"
            });
        },
        allManage: function () {

            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/quotation/management"
            });
        },

        create: function (quotation) {

            return $http({
                method: 'post',
                url: BACKEND.LOCATION + "/quotation",
                data: quotation
            });

        },

        update: function (quotation) {
            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quotation",
                data: quotation
            });

        },

        find: function (quotationId) {

            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/quotation/" + quotationId
            });
        },

        delete: function (quotationId) {

            return $http({
                method: 'delete',
                url: BACKEND.LOCATION + "/quotation/" + quotationId
            });
        },

        addProductsToQuotation: function (quotationId, productsToAdd) {

            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quotation/" + quotationId + "/quoteproducts",
                data: productsToAdd
            });
        },

        updateProductInQuotation: function (quotationId, product) {

            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quoteproduct",
                data: product
            });
        },

        removeProductFromQuotation: function (product) {
            return $http({
                method: 'delete',
                url: BACKEND.LOCATION + "/quoteproduct/" + product.id
            });
        },

        getQuotePrice: function (quoteId) {

            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/quotation/" + quoteId + "/price",
            });
        },
        finalizeQuotation: function (quoteId) {

            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quotation/" + quoteId + "/finalize",
            });
        },
        reviewQuotation: function (quoteId) {

            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quotation/" + quoteId + "/review",
            });
        },
        presentQuotation: function (quoteId) {

            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quotation/" + quoteId + "/present",
            });
        },

        acceptQuotation: function (quoteId) {

            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quotation/" + quoteId + "/accept",
            });
        },

        declineQuotation: function (quoteId) {

            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quotation/" + quoteId + "/decline",
            });
        },

        cancelQuotation: function (quoteId) {

            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/quotation/" + quoteId + "/cancel",
            });
        },

        getInvoiceGenerationURL: function (quoteId) {

            return BACKEND.LOCATION + "/quotation/" + quoteId + "/invoice/generate";
        },
    };
}]);
