'use-strict';

angular.module("ahlowebinterface")

  .factory("ProductService", ["$http", "BACKEND", function ProductService($http, BACKEND) {
    return {
      all: function () {
        return $http({
          method: 'get',
          url: BACKEND.LOCATION + "/product"
        });
      },

      create: function (product) {
        return $http({
          method: 'post',
          url: BACKEND.LOCATION + "/product",
          data: product
        });

      },

      update: function (product) {
        return $http({
          method: 'put',
          url: BACKEND.LOCATION + "/product",
          data: product
        });

      },

      find: function (productId) {

        return $http({
          method: 'get',
          url: BACKEND.LOCATION + "/product/" + productId
        });
      },

      delete: function (productId) {

        return $http({
          method: 'delete',
          url: BACKEND.LOCATION + "/product/" + productId
        });
      },

      getAvailableProductsForQuotation: function (quotationId) {

        return $http({
          method: 'get',
          url: BACKEND.LOCATION + "/product/visible/" + quotationId
        });
      },

      getProductImages: function (productId) {

        return $http({
          method: 'get',
          url: BACKEND.LOCATION + "/product/" + productId + "/images"
        });

      },

      resolveImage: function (imageId) {

        return $http({
          method: 'get',
          url: BACKEND.LOCATION + "/productpicture/" + imageId + "/resolve"
        });
      },

      getImageUploadURL: function (productId) {

        return BACKEND.LOCATION + "/upload/product/" + productId + "/image";
      },

      setPrimaryProductPicture: function (productId, imageId) {
        return $http({
          method: 'put',
          url: BACKEND.LOCATION + "/productpicture/primary",
          params: {
            product: productId,
            picture: imageId,
          },
        });
      },
      deleteProductPicture: function (imageId) {
        return $http({
          method: 'delete',
          url: BACKEND.LOCATION + "/productpicture/" + imageId
        });
      },
      getPrimaryPicture: function (productId) {
        return $http({
          method: 'get',
          url: BACKEND.LOCATION + "/product/" + productId + "/image/primary"
        });
      },
      getOtherPictures: function (productId) {
        return $http({
          method: 'get',
          url: BACKEND.LOCATION + "/product/" + productId + "/image/other"
        });
      },
      allWithThumbs: function () {
        return $http({
          method: 'get',
          url: BACKEND.LOCATION + "/product/withthumbs"
        });
      }
    };
  }]);
