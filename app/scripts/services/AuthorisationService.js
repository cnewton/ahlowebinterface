'use-strict';

angular.module('ahlowebinterface')

.service('AuthorisationService', [
    '$http',
    'BACKEND',
    '$window',
    '$base64',
  function ($http, BACKEND, $window, $base64) {

        this.authorise = function (user, state) {

            var auth = false;
            if (state.access.permissions === undefined || state.access.permissions.length === 0) {
                auth = true;
            } else if (state.access.permissions.length === 1) {
                angular.forEach(user.roles, function (role) {
                    if (role.role === state.access.permissions[0]) {
                        auth = true;
                    }
                });
            } else {
                auth = true;
                angular.forEach(state.access.permissions, function (requiredPermission) {
                    angular.forEach(user.roles, function (role) {
                        if (role.role !== requiredPermission) {
                            auth = false;
                        }
                    });
                });
            }

            return auth;
        };
}

]);
