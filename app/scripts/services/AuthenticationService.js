'use-strict';

angular.module('ahlowebinterface')

.service('AuthenticationService', [
    '$http',
    'BACKEND',
    '$window',
    '$base64',
  function ($http, BACKEND, $window, $base64) {

        this.login = function (username, password) {
            var loginState = {
                failed: false,
                error: '',
            };

            return $http({
                method: 'post',
                url: BACKEND.LOCATION + "/j_spring_security_check",
                data: "username=" + username + "&password=" + password,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(

                function successCallback(response) {
                    $window.sessionStorage.setItem(BACKEND.AUTH_HEADER, response.headers(BACKEND.AUTH_HEADER));
                    $window.sessionStorage.setItem("currentUser", $base64.decode($window.sessionStorage.getItem(BACKEND.AUTH_HEADER).split(".")[0]));
                    return loginState;
                },
                function errorCallback(response) {

                    loginState.failed = true;
                    if (response.status === 401) {
                        loginState.error = "Incorrect username or password";
                    } else {
                        loginState.error = "Unable to contact server";
                    }
                    return loginState;
                }
            );
        };

        //Backend does not require any notification.
        this.logout = function () {
            $window.sessionStorage.removeItem("currentUser");
            $window.sessionStorage.removeItem(BACKEND.AUTH_HEADER);
        };

        this.getCurrentUser = function () {
            if ($window.sessionStorage.getItem("currentUser") !== null) {
                return JSON.parse($window.sessionStorage.getItem("currentUser"));
            } else
                return null;
        };

}]);
