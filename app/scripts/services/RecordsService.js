'use-strict';

angular.module("ahlowebinterface")

.factory("RecordsService", ["$http", "BACKEND", function RecordsService($http, BACKEND) {
    return {
        all: function () {

            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/records/quotation"
            });
        },
        find: function (recordId) {

            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/records/quotation/" + recordId
            });
        },
        getInvoiceGenerationURL: function (quoteId) {

            return BACKEND.LOCATION + "/records/" + quoteId + "/invoice/generate";
        },
    };
}]);
