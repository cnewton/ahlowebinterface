'use-strict';

angular.module("ahlowebinterface")

.factory("BuyerService", ["$http", "BACKEND", function BuyerService($http, BACKEND) {
    return {
        all: function () {
            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/buyer"
            });
        },

        create: function (buyer) {
            return $http({
                method: 'post',
                url: BACKEND.LOCATION + "/buyer",
                data: buyer
            });

        },

        update: function (buyer) {
            return $http({
                method: 'put',
                url: BACKEND.LOCATION + "/buyer",
                data: buyer
            });

        },

        find: function (buyerId) {

            return $http({
                method: 'get',
                url: BACKEND.LOCATION + "/buyer/" + buyerId
            });
        },

        delete: function (buyerId) {

            return $http({
                method: 'delete',
                url: BACKEND.LOCATION + "/buyer/" + buyerId
            });
        },
    };
}]);
