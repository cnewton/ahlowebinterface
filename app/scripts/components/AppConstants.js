'use-strict';

angular.module("ahlowebinterface")

.constant('PERMISSION_LEVEL', Object.freeze({
    ADMIN: 'ROLE_ADMIN',
    USER: 'ROLE_USER',
}))

.value('BACKEND', {
    LOCATION: '',
    AUTH_HEADER: 'X-AUTH-TOKEN',
})

// Set the backend location from the current host.
.run(['$location', 'BACKEND', function ($location, BACKEND) {
    BACKEND.LOCATION = 'http://' + $location.host() + ':8080/ahlopm';
}]);
