'use-strict';

angular.module('ahlowebinterface')

.controller('LoginController', ['$scope', '$state', 'AuthenticationService', function ($scope, $state, AuthenticationService) {

    $scope.loginState = {
        failed: false,
        error: ''
    };

    $scope.login = function () {
        AuthenticationService.login($scope.username, $scope.password).then(
            function (loginState) {
                $scope.loginState = loginState;
                if (!$scope.loginState.failed) {
                    $state.go('page.home');
                }
            }
        );
    };
}]);
