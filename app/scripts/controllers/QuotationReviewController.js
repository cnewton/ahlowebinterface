'use strict';

angular.module("ahlowebinterface")

  .controller("QuotationReviewController", [
    '$scope',
    '$state',
    'uiGridConstants',
    'QuotationService',
    '$timeout',
    '$window',
    function ($scope, $state, uiGridConstants, QuotationService, $timeout, $window) {

      /*Scope variables*/

      //Angular busy promises
      $scope.populatePromise = [];
      $scope.trackedPromises = [];

      /*Initial data population*/
      populate();

      /* Display configuration, initialization*/
      $scope.gridOptions = {
        data: 'quotations',
        enableSorting: true,
        enableCellEdit: true,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        noUnselect: true,
        minRowsToShow: 10
      };
      $scope.gridOptions.columnDefs = [
        {
          name: 'name',
          enableCellEdit: true,
          width: '*'
        },
        {
          name: 'buyer name',
          field: 'buyer.name',
          enableCellEdit: false,
          width: '*'
        },
        {
          name: 'buyer company',
          field: 'buyer.company',
          enableCellEdit: false,
          width: '*'
        },
        {
          name: 'lastModified',
          enableCellEdit: false,
          cellFilter: 'date:"yyyy/MM/dd hh:mm a"',
          sort: {
            direction: uiGridConstants.DESC,
            priority: 0
          },
          width: '*'
        },
        {
          name: 'revision',
          field: 'revisionNo',
          width: '*'
        },
        {
          name: 'price',
          field: 'discountedPrice',
          cellFilter: 'currency',
          width: '*'
        }
      ];

      /*Local helper functions*/
      var calculateDiscountedPrice = function (quote) {
        quote.discountedPrice = (quote.price - quote.discount);
      };

      //Move selection to the newly created Quotation
      var gridSelectQuote = function (quote) {
        //Timeout to allow grid api actions to take place beforehand.
        $timeout(function () {
          var arrayPos = $scope.quotations.indexOf(quote);
          $scope.gridApi.selection.selectRow($scope.quotations[arrayPos]);
          $scope.gridApi.core.scrollTo($scope.quotations[arrayPos]);
        }, 50);
      };

      var updateQuotation = function (quote) {
        QuotationService.update(quote).then(
          function successCallback(response) {
            var arrayPos = $scope.quotations.indexOf(quote);
            calculateDiscountedPrice(response.data);
            $scope.quotations[arrayPos] = response.data;
            gridSelectQuote(response.data);
          },
          function errorCallback(response) {
            $window.alert(response.data);
          }
        );
      };

      function populate() {
        $scope.populatePromise.push(
          QuotationService.allReview().then(
            function successCallback(response) {
              $scope.quotations = response.data;
              angular.forEach($scope.quotations, function (quote) {
                calculateDiscountedPrice(quote);
              });
            },
            function errorCallback() {
              $window.alert("Failed to contact server");
              $state.go("page.home");
            }
          )
        );
      }

      //Grid Handler//
      $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;

        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
          $scope.selectQuotation(row.entity);
        });

        gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
          if (newValue !== oldValue) {
            if (colDef.name === 'price') {
              rowEntity.discount = (rowEntity.price - newValue);
            }
            updateQuotation(rowEntity);
          }
        });
      };

      /*Event handlers*/
      $scope.selectQuotation = function (quotation) {
        $scope.entitySelected = true;
        $scope.selectedEntry = quotation;
      };

      $scope.calculatePrice = function (quote) {

        var promise = QuotationService.getQuotePrice(quote.id).then(
          function successCallback(response) {
            quote.price = response.data;
            calculateDiscountedPrice(quote);
          });
        $scope.trackedPromises.push(promise);
      };

      $scope.finalizeQuotation = function (quote) {
        if ($window.confirm("Are you sure you are ready to finalize?")) {
          var promise = QuotationService.finalizeQuotation(quote.id).then(
            function successCallback() {
              $state.go('page.quotationsmanage');
            },
            function errorCallback(response) {
              $window.alert(response.data);
            }
          );
          $scope.trackedPromises.push(promise);
        }
      };

      $scope.newQuotation = function () {
        var quotation = {
          name: "New Quotation",
          status: "REVIEW",
          price: 0,
          discount: 0,
          remarks: "",
          id: 0
        };
        QuotationService.create(quotation).then(
          function successCallback(response) {
            calculateDiscountedPrice(response.data);
            $scope.quotations.push(response.data);
            gridSelectQuote(response.data);
          },
          function errorCallback(response) {
            $window.alert(response.data);
          }
        );
      };

      $scope.deleteQuotation = function () {
        if ($window.confirm("Are you sure you want to delete this?")) {
          var arrayPos = $scope.quotations.indexOf($scope.selectedEntry);
          $scope.quotations.splice(arrayPos, 1);
          $scope.entitySelected = false;
          QuotationService.delete($scope.selectedEntry.id);
        }
      };
    }
  ]);
