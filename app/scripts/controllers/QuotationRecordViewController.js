'use-strict';

angular.module("ahlowebinterface")

.controller("QuotationRecordViewController", [
    '$scope',
    '$stateParams',
    'uiGridConstants',
    'RecordsService',
    '$state',
    '$window',
    function ($scope, $stateParams, uiGridConstants, RecordsService, $state, $window) {

        /*Initial Page Setup*/

        //Angular busy promises
        $scope.populatePromise = [];

        /*Initial data population*/
        populate();

        /* Display configuration, initialization*/
        $scope.gridOptions = {
            data: 'quotation.products',
            enableSorting: true,
            enableCellEdit: false,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            multiSelect: false,
            noUnselect: true,
            minRowsToShow: 10
        };
        $scope.gridOptions.columnDefs = [
            {
                name: 'product code',
                field: 'product.productCode',
                width: '*',
            },
            {
                name: 'name',
                field: 'product.name',
                width: '*',
            },
            {
                name: 'description',
                field: 'product.description',
                width: '*',
            },
            {
                name: 'quantity',
                field: 'quantity',
                width: '*',
            },
            {
                name: 'price',
                field: 'discountedPrice',
                width: '*',
            }
        ];

        /*Local Helper Functions*/

        function populate() {
            $scope.populatePromise.push(
                RecordsService.find($stateParams.quoteId).then(
                    function successCallback(response) {
                        $scope.quotation = JSON.parse(response.data.quotation);
                        calculatePrices($scope.quotation.products);
                        $scope.selectedBuyer = $scope.quotation.buyer;
                    },
                    function errorCallback(response) {
                        $window.alert("Requested quotation does not exist");
                        $state.go('page.home');
                    }
                )
            );
        }

        var calculatePrices = function (products) {
            angular.forEach(products, function (product) {
                product.discountedPrice = (product.product.price - product.discount);
            });
        };


        /*Event handlers*/

        $scope.gridOptions.onRegisterApi = function (gridApi) {
            $scope.gridApi = gridApi;

            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.entitySelected = row.isSelected;
                $scope.selectedEntry = row.entity;
            });

        };

}]);
