'use strict';

angular.module("ahlowebinterface")

  .config(["LightboxProvider", function (LightboxProvider) {

    //Custom lightbox template
    LightboxProvider.templateUrl = 'views/custom-lightbox-view.html';
  }])

  .controller("ProductListController", [
    '$scope',
    'uiGridConstants',
    'ProductService',
    'Lightbox',
    '$state',
    '$window',
    '$timeout',
    function ($scope, uiGridConstants, ProductService, Lightbox, $state, $window, $timeout) {

      $scope.resolvedImages = [];
      $scope.trackedPromises = [];

      /*Initial data population*/
      populate();

      /* Display configuration, initialization*/
      $scope.gridOptions = {
        data: 'products',
        enableSorting: true,
        enableFiltering: true,
        enableCellEdit: true,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        noUnselect: true,
        rowHeight: 75,
        minRowsToShow: 5
      };
      $scope.gridOptions.columnDefs = [
        {
          name: 'productCode',
          minWidth: '150'
        },
        {
          name: 'description',
          minWidth: '200'
        },
        {
          name: 'style',
          minWidth: '100'
        },
        {
          name: 'finish',
          minWidth: '100'
        },
        {
          name: 'price',
          minWidth: '120',
          enableFiltering: false,
          cellFilter: 'currency'
        },
        {
          name: 'image',
          enableFiltering: false,
          enableSorting: false,
          cellTemplate: '<div ng-click="grid.appScope.showImage(row.entity)" style="cursor: pointer;">' +
          '<div ng-if="!row.entity.thumbnail"><img src="/images/no_image.png" height="75" width="100"></div>' +
          '<div ng-if="row.entity.thumbnail"><img ng-src="data:image/png;base64,{{row.entity.thumbnail}}"></div>' +
          '</div>'
        }
      ];

      /*Local helper functions*/
      function populate() {
        $scope.trackedPromises.push(
          ProductService.allWithThumbs().then(
            function successCallback(response) {
              $scope.products = response.data;
            },
            function errorCallback() {
              $window.alert("Failed to contact server");
              $state.go('page.home');
            }
          )
        );
      }

      var hideProduct = function (product) {
        var arrayPos = $scope.products.indexOf(product);
        $scope.products.splice(arrayPos, 1);
      };

      //Move selection to the newly created Quotation
      var gridSelectProduct = function (product) {
        //Timeout to allow grid api actions to take place beforehand.
        $timeout(function () {
          var arrayPos = $scope.products.indexOf(product);
          $scope.gridApi.selection.selectRow($scope.products[arrayPos]);
          $scope.gridApi.core.scrollTo($scope.products[arrayPos]);
        }, 50);
      };

      /*Event handlers*/

      $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;

        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
          $scope.entitySelected = row.isSelected;
          $scope.selectedEntry = row.entity;
        });

        gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
          if (newValue !== oldValue) {
            console.log(rowEntity);
            ProductService.update(rowEntity).then(
              function successCallback(response) {
                gridSelectProduct(response.data);
              }
            );
          }
        });
      };

      $scope.showImage = function (product) {

        $scope.resolvedImages.length = 0;

        //Get the primary image for this product.
        ProductService.getPrimaryPicture(product.id).then(
          function successCallback(response) {
            if (response.data) {
              var primary = response.data;
              var resolvedImage = {
                imageId: primary.id,
                productId: product.id,
                url: "data:image/" + primary.encodeType + ";base64," + primary.picture,
                isPrimary: true
              };
              $scope.resolvedImages.push(resolvedImage);

              //Get all other images for this product
              ProductService.getOtherPictures(product.id).then(
                function successCallback(response) {
                  response.data.some(function (picture) {
                    var resolvedImage = {
                      imageId: picture.id,
                      productId: product.id,
                      url: "data:image/" + picture.encodeType + ";base64," + picture.picture,
                    };
                    $scope.resolvedImages.push(resolvedImage);
                  });
                }
              );
              //Open the lightbox (images will continue to be added as they are resolved).
              Lightbox.openModal($scope.resolvedImages, 0);
            }
            //If no primary image, display missing image icon
            else {
              var no_image = [{
                url: "/images/no_image.png",
                productId: product.id
              }];

              Lightbox.openModal(no_image, 0);
            }
          }
        );
      };


      $scope.newProduct = function () {
        var product = {
          productCode: "New Product",
          price: 0,
          id: 0
        };
        ProductService.create(product).then(
          function successCallback(response) {
            $scope.products.push(response.data);
            gridSelectProduct(response.data);
          }
        );
      };

      $scope.deleteProduct = function () {
        ProductService.delete($scope.selectedEntry.id);
        hideProduct($scope.selectedEntry);
        $scope.entitySelected = false;
        $scope.selectedEntry = {};
      };
    }
  ]);
