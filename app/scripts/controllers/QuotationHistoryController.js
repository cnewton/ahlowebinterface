'use-strict';

angular.module("ahlowebinterface")

  .controller("QuotationHistoryController", [
    '$scope',
    '$state',
    'uiGridConstants',
    'RecordsService',
    '$window',
    function ($scope, $state, uiGridConstants, RecordsService, $window) {

      /*Scope variables*/

      //Angular busy promises
      $scope.populatePromise = [];
      $scope.trackedPromises = [];

      /*Initial data population*/
      populate();

      /* Display configuration, initialization*/
      $scope.gridOptions = {
        data: 'records',
        enableSorting: true,
        enableCellEdit: false,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        noUnselect: true,
        minRowsToShow: 10
      };
      $scope.gridOptions.columnDefs = [
        {
          name: 'name',
          field: 'quotation.name',
          width: '*'
        },
        {
          name: 'buyer name',
          field: 'quotation.buyer.name',
          width: '*'
        },
        {
          name: 'buyer company',
          field: 'quotation.buyer.company',
          width: '*'
        },
        {
          name: 'lastModified',
          field: 'quotation.lastModified',
          cellFilter: 'date:"yyyy/MM/dd hh:mm a"',
          sort: {
            direction: uiGridConstants.DESC,
            priority: 0
          },
          width: '*'
        },
        {
          name: 'status',
          width: '*'
        },
        {
          name: 'price',
          field: 'discountedPrice',
          width: '*',
          cellFilter: 'currency'
        }
      ];

      /*Local helper functions*/
      var calculateDiscountedPrice = function (quote) {
        quote.discountedPrice = (quote.price - quote.discount);
      };


      function populate() {
        $scope.populatePromise.push(
          RecordsService.all().then(
            function successCallback(response) {
              $scope.records = response.data;
              angular.forEach($scope.records, function (record) {
                record.quotation = JSON.parse(record.quotation);
                calculateDiscountedPrice(record.quotation);
                record.discountedPrice = record.quotation.discountedPrice;
              });
            },
            function errorCallback(response) {
              $window.alert("Failed to contact server");
              $state.go("page.home");
            }
          )
        );
      }

      //Grid Handler//
      $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;

        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
          $scope.selectQuotation(row.entity);
        });
      };

      /*Event Handlers */
      $scope.selectQuotation = function (quotation) {
        $scope.entitySelected = true;
        $scope.selectedEntry = quotation;
      };

      $scope.downloadXLS = function (quote) {
        $window.open(RecordsService.getInvoiceGenerationURL(quote.id));
      };
    }
  ]);
