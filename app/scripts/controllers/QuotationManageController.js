'use strict';

angular.module("ahlowebinterface")

  .controller("QuotationManageController", [
    '$scope',
    '$state',
    'uiGridConstants',
    'QuotationService',
    '$window',
    function ($scope, $state, uiGridConstants, QuotationService, $window) {

      /*Scope variables*/

      //Angular busy promises
      $scope.populatePromise = [];
      $scope.trackedPromises = [];

      /*Initial data population*/
      populate();

      /* Display configuration, initialization*/
      $scope.gridOptions = {
        data: 'quotations',
        enableSorting: true,
        enableCellEdit: false,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        noUnselect: true,
        minRowsToShow: 10
      };
      $scope.gridOptions.columnDefs = [
        {
          name: 'name',
          width: '*'
        },
        {
          name: 'buyer name',
          field: 'buyer.name',
          width: '*'
        },
        {
          name: 'buyer company',
          field: 'buyer.company',
          width: '*'
        },
        {
          name: 'lastModified',
          cellFilter: 'date:"yyyy/MM/dd hh:mm a"',
          sort: {
            direction: uiGridConstants.DESC,
            priority: 0
          },
          width: '*'
        },
        {
          name: 'status',
          width: '*'
        },
        {
          name: 'price',
          field: 'discountedPrice',
          width: '*',
          cellFilter: 'currency'
        }
      ];

      /*Local helper functions*/
      var calculateDiscountedPrice = function (quote) {
        quote.discountedPrice = (quote.price - quote.discount);
      };


      function populate() {
        $scope.populatePromise.push(
          QuotationService.allManage().then(
            function successCallback(response) {
              $scope.quotations = response.data;
              if ($scope.quotations.length > 0) {
                $scope.selectedEntry = $scope.quotations[0];
                $scope.entitySelected = true;
              }
              angular.forEach($scope.quotations, function (quote) {
                calculateDiscountedPrice(quote);
              });

            },
            function errorCallback() {
              $window.alert("Failed to contact server");
              $state.go("page.home");
            }
          )
        );
      }

      //Grid Handler//
      $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;

        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
          $scope.selectQuotation(row.entity);
        });
      };

      /*Event handlers*/

      $scope.selectQuotation = function (quotation) {
        $scope.entitySelected = true;
        $scope.selectedEntry = quotation;
      };

      $scope.canReview = function (quote) {
        if (!quote) return false;
        return (quote.status !== undefined && quote.status !== "PRESENTING");
      };

      $scope.reviewQuotation = function (quote) {
        if ($window.confirm("Are you sure you want to review?")) {
          var promise = QuotationService.reviewQuotation(quote.id).then(
            function successCallback(response) {
              $window.alert(response.data);
              $state.go('page.quotationsreview');
            },
            function errorCallback(response) {
              $window.alert(response.data);
            }
          );
          $scope.trackedPromises.push(promise);
        }
      };

      $scope.canPresent = function (quote) {
        if (!quote) return false;
        return (quote.status === "FINALIZING");
      };

      $scope.presentQuotation = function (quote) {
        if ($window.confirm("Are you sure you are ready to present?")) {
          var promise = QuotationService.presentQuotation(quote.id).then(
            function successCallback() {
              populate();
            },
            function errorCallback(response) {
              $window.alert(response.data);
            }
          );
          $scope.trackedPromises.push(promise);
        }
      };

      $scope.canProgress = function (quote) {
        if (!quote) return false;
        return (quote.status === "PRESENTING");
      };

      $scope.acceptQuotation = function (quote) {
        if ($window.confirm("Are you sure this quotation was accepted?")) {
          var promise = QuotationService.acceptQuotation(quote.id).then(
            function successCallback() {
              populate();
            },
            function errorCallback(response) {
              $window.alert(response.data);
            }
          );
          $scope.trackedPromises.push(promise);
        }
      };

      $scope.declineQuotation = function (quote) {
        if ($window.confirm("Are you sure this quotation was declined?")) {
          var promise = QuotationService.declineQuotation(quote.id).then(
            function successCallback() {
              populate();
            },
            function errorCallback(response) {
              $window.alert(response.data);
            }
          );
          $scope.trackedPromises.push(promise);
        }
      };

      $scope.cancelQuotation = function (quote) {
        if ($window.confirm("Are you sure you want to cancel? (THIS WILL DELETE THE STORED RECORD)")) {
          var promise = QuotationService.cancelQuotation(quote.id).then(
            function successCallback() {
              populate();
            },
            function errorCallback(response) {
              $window.alert(response.data);
            }
          );
          $scope.trackedPromises.push(promise);
        }
      };

      $scope.downloadXLS = function (quote) {
        $window.open(QuotationService.getInvoiceGenerationURL(quote.id));
      };
    }
  ]);
