'use strict';

angular.module("ahlowebinterface")
  .controller('NavbarController', [
    '$scope',
    '$state',
    '$rootScope',
    'AuthenticationService',
    function ($scope, $state, $rootScope, AuthenticationService) {

      $scope.currentStateName = $state.current.name;

      $scope.navBarGroups = [
        {
          title: 'Quotations',
          viewable: function () {
            return true;
          },
          groupItems: [
            {
              sref: 'page.quotationsreview',
              title: 'Review Quotations',
              viewable: function () {
                return true;
              }
            },
            {
              sref: 'page.quotationsmanage',
              title: 'Manage Quotations',
              viewable: function () {
                return true;
              }
            },
            {
              sref: 'page.quotationshistory',
              title: 'Historical Quotations',
              viewable: function () {
                return true;
              }
            }
            //New group members here
          ]
        }
        //New groups here
      ];
      $scope.navBarItems = [
        {
          sref: 'page.productlist',
          title: 'Products',
          viewable: function () {
            return true;
          }
        },
        {
          sref: 'page.accounts',
          title: 'Accounts',
          viewable: function () {
            return true;
          }
        }
        //New items here
      ];

      $scope.navBarTasks = [
        {
          title: 'Logout',
          clicked: function () {
            AuthenticationService.logout();
            $state.go('login');
          },
          viewable: function () {
            return true;
          }
        }
        //New tasks here
      ];


// When the page changes, update the current state name
      $rootScope.$on('$stateChangeStart', function (event, toState) {
        $scope.currentStateName = toState.name;
      });
    }])
;
