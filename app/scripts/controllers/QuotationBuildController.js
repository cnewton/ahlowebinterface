'use strict';

angular.module("ahlowebinterface")

  .controller("QuotationBuildController", [
    '$scope',
    '$stateParams',
    'uiGridConstants',
    'QuotationService',
    '$state',
    '$window',
    function ($scope, $stateParams, uiGridConstants, QuotationService, $state, $window) {

      /*Initial Page Setup*/
      $scope.searchingForBuyer = false;
      $scope.searchingForProducts = false;
      $scope.selectedBuyer = {};
      $scope.buyerSelected = false;
      $scope.productsToAdd = [];

      //Angular busy promises
      $scope.populatePromise = [];
      $scope.trackedPromises = [];

      /*Initial data population*/
      populate();

      /* Display configuration, initialization*/
      $scope.gridOptions = {
        data: 'quotation.products',
        enableSorting: true,
        enableCellEdit: true,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: false,
        noUnselect: true,
        minRowsToShow: 10
      };
      $scope.gridOptions.columnDefs = [
        {
          name: 'product code',
          field: 'product.productCode',
          enableCellEdit: false,
          minWidth: '150'
        },
        {
          name: 'description',
          field: 'product.description',
          enableCellEdit: false,
          minWidth: '200'
        },
        {
          name: 'quantity',
          field: 'quantity',
          minWidth: '100'
        },
        {
          name: 'price',
          field: 'discountedPrice',
          minWidth: '120',
          cellFilter: 'currency'
        }
      ];

      /*Local Helper Functions*/

      function populate() {
        $scope.populatePromise.push(
          QuotationService.find($stateParams.quoteId).then(
            function successCallback(response) {
              $scope.quotation = response.data;
              calculatePrices($scope.quotation.products);
              $scope.selectedBuyer = $scope.quotation.buyer;
            },
            function errorCallback(response) {
              $window.alert(response.data);
              $state.go('page.home');
            }
          )
        );
      }

      var calculatePrices = function (products) {
        angular.forEach(products, function (product) {
          product.discountedPrice = (product.product.price - product.discount);
        });
      };

      var hideProduct = function (product) {
        var arrayPos = $scope.quotation.products.indexOf(product);
        $scope.quotation.products.splice(arrayPos, 1);
      };

      /*Event handlers*/

      $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;

        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
          $scope.entitySelected = row.isSelected;
          $scope.selectedEntry = row.entity;
        });

        gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
          if (newValue !== oldValue) {
            if (colDef.name === 'price') {
              rowEntity.discount = (rowEntity.product.price - newValue);
            }
            QuotationService.updateProductInQuotation($scope.quotation.id, rowEntity).then(
              function successCallback() {
                calculatePrices($scope.quotation.products);
              }
            );
          }
        });
      };


      $scope.seeBuyerList = function () {
        $scope.searchingForBuyer = true;
      };

      $scope.seeProductSearch = function () {
        $scope.searchingForProducts = true;
      };

      $scope.updateBuyer = function () {
        QuotationService.update($scope.quotation);
        $scope.searchingForBuyer = false;
      };

      $scope.updateProducts = function () {
        $scope.trackedPromises.push(
          QuotationService.addProductsToQuotation($scope.quotation.id, $scope.productsToAdd).then(
            function successCallback() {
              populate();
            },
            function errorCallback(response) {
              $window.alert(response.data);
            }
          )
        );
        $scope.searchingForProducts = false;
        $scope.productsToAdd = [];
      };

      $scope.removeProduct = function () {
        $scope.trackedPromises.push(QuotationService.removeProductFromQuotation($scope.selectedEntry));
        hideProduct($scope.selectedEntry);
        $scope.selectedEntry = {};
        $scope.entitySelected = false;
      };

      $scope.updateRemarks = function () {
        QuotationService.update($scope.quotation);
      };
    }]);
