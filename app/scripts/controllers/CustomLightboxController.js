'use strict';

angular.module("ahlowebinterface")

  .controller("CustomLightboxController", [
    '$scope',
    '$window',
    'ProductService',
    'FileUploader',
    'BACKEND',
    '$timeout',
    function ($scope, $window, ProductService, FileUploader, BACKEND, $timeout) {

      $scope.uploading = false;

      $scope.lightbox = {};

      $scope.uploadProgress = 0;

      $scope.uploader = new FileUploader({
        headers: {
          'X-AUTH-TOKEN': $window.sessionStorage.getItem(BACKEND.AUTH_HEADER)
        }
      });

      /*Runs before upload.*/
      $scope.uploadImage = function (productId) {
        $scope.uploader.url = ProductService.getImageUploadURL(productId);
        $scope.currentProductId = productId;
      };

      /*Delete the image, remove it from the lightbox, close lightbox if no more images.*/
      $scope.deleteImage = function (image) {
        ProductService.deleteProductPicture(image.imageId);
        if ($scope.lightbox.images.length > 1) {
          $scope.lightbox.images.splice($scope.lightbox.images.indexOf(image), 1);
          $scope.lightbox.firstImage();
          //Timeout to allow index switch first.
          $timeout(function () {
            $scope.lightbox.setImages($scope.lightbox.images);
          }, 50);
        } else {
          $scope.lightbox.closeModal();
        }
      };

      /*Mark all images as non primary, mark the new primary*/
      $scope.setPrimaryImage = function (image) {
        ProductService.setPrimaryProductPicture(image.productId, image.imageId).then(
          function successCallback(response) {
            angular.forEach($scope.lightbox.images, function (oldImage) {
              oldImage.isPrimary = false;
            });
            image.isPrimary = true;
          }
        );
      };

      $scope.uploader.onAfterAddingFile = function (fileItem) {
        $scope.uploading = true;
        $scope.uploader.uploadItem(fileItem);
      };

      $scope.uploader.onProgressAll = function (progress) {
        $scope.uploadProgress = progress;
      };

      /*After upload is complete, load the image into the lightbox and switch to it.*/
      $scope.uploader.onCompleteAll = function () {
        $scope.uploading = false;
        $scope.uploadProgress = 0;
        //If the lightbox is still open.
        var oldIds = [];
        angular.forEach($scope.lightbox.images, function (image) {
          oldIds.push(image.imageId);
        });
        //If the first image had no id (it was the placeholder)
        if (!oldIds[0]) {
          //Remove the placeholder.
          $scope.lightbox.images = [];
        }
        ProductService.getProductImages($scope.currentProductId).then(
          function successCallback(response) {
            angular.forEach(response.data, function (newId) {
              //Find the new addition. (It wont be part of the old id array)
              //Also check if the lightbox is still open
              if (oldIds.indexOf(newId) === -1 && $scope.lightbox.imageUrl) {
                ProductService.resolveImage(newId).then(
                  function successCallback(response) {
                    var newImage = {
                      imageId: newId,
                      productId: $scope.currentProductId,
                      url: "data:" + response.headers("Content-Type") + ";base64," + response.data,
                    };
                    //Check if the lightbox is still open.
                    if ($scope.lightbox.imageUrl) {
                      $scope.lightbox.images.push(newImage);
                      $scope.lightbox.setImages($scope.lightbox.images);
                      $scope.lightbox.setImage($scope.lightbox.images.indexOf(newImage));
                    }
                  }
                );
              }
            });
          }
        );
      };

      /*Enables the passing of the lightbox to this controller.*/
      $scope.init = function (lightbox) {
        $scope.lightbox = lightbox;
        //Watch for the lightbox being closed, cancel any uploads in progress when closed.
        $scope.$watch("lightbox.imageUrl", function handleLightboxClose(newUrl, oldUrl) {
          if (newUrl === null) {
            $scope.uploader.cancelAll();
          }
        });
      };

    }
  ]);
