'use-strict';

angular.module("ahlowebinterface")

.controller("AccountsController", [
    '$scope',
    'AccountsService',
    '$window',
    '$state',
    'PERMISSION_LEVEL',
    'AuthenticationService',
    function ($scope, AccountsService, $window, $state, PERMISSION_LEVEL, AuthenticationService) {

        $scope.currentUserIsAdmin = false;
        $scope.trackedPromises = [];
        $scope.roles = PERMISSION_LEVEL;
        $scope.newUser = {};

        angular.forEach(AuthenticationService.getCurrentUser().roles, function (role) {
            if (role.role === PERMISSION_LEVEL.ADMIN) {
                $scope.currentUserIsAdmin = true;
            }
        });

        /*Initial Data Population*/
        populate();

        /*Local helper functions*/
        function populate() {
            $scope.trackedPromises.push(AccountsService.account().then(
                function successCallback(response) {
                    $scope.selectedUser = response.data;
                },
                function errorCallback(response) {
                    $window.alert("Unable to contact server");
                    $state.go('page.home');
                }
            ));
            if ($scope.currentUserIsAdmin) {
                $scope.trackedPromises.push(AccountsService.all().then(
                    function successCallback(response) {
                        $scope.users = response.data;
                    }
                ));
            }
        }

        /*Event handlers*/

        $scope.updateDetails = function () {
            if ($scope.selectedUser.password && $scope.selectedUser.password.length < 8) {
                $scope.updateFailed = true;
                $scope.updateError = "New Password is too short minimum is 8 characters";
            } else if ($scope.selectedUser.password !== $scope.selectedUser.repeatPassword) {
                $scope.updateFailed = true;
                $scope.updateError = "Passwords do not match";
            } else {
                $scope.updateFailed = false;
                $scope.updateError = "";

            }
            AccountsService.update($scope.selectedUser).then(
                function successCallback(response) {
                    $scope.selectedUser = response.data;
                },
                function errorCallback(response) {
                    $scope.updateFailed = true;
                    $scope.updateError = response.data;
                }
            );
            if ($scope.selectedUser.password && !$scope.updateFailed) {
                var promise;
                if ($scope.currentUserIsAdmin) {
                    promise = AccountsService.adminChangePassword($scope.selectedUser.id, $scope.selectedUser.password);
                } else {
                    promise = AccountsService.userChangePassword($scope.selectedUser.id, $scope.selectedUser.oldPassword, $scope.selectedUser.password);
                }
                promise.then(function successCallback(response) {
                    $scope.updateFailed = false;
                    $scope.updateError = "";
                }, function errorCallback(response) {
                    $scope.updateFailed = true;
                    $scope.updateError = response.data;
                });
            }
        };

        $scope.createUser = function (form) {
            if (form.$valid) {
                $scope.newUser.roles = [];
                $scope.newUser.enabled = true;
                angular.forEach(PERMISSION_LEVEL, function (permission) {
                    if ($scope.newUser[permission]) {
                        var role = {
                            id: 0,
                            role: permission,
                        };
                        $scope.newUser.roles.push(role);
                    }
                });
                AccountsService.create($scope.newUser).then(
                    function successCallback(response) {
                        $scope.newUser = {};
                        populate();
                    },
                    function errorCallback(response) {
                        $scope.createFailed = true;
                        $scope.createError = response.data;
                    }
                );
            }
        };

        $scope.changeSelection = function (selection) {
            $scope.selectedUser = selection;
        };

    }
]);
