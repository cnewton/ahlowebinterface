'use strict';

angular.module("ahlowebinterface")

  .controller("ProductSearchController", [
    '$scope',
    'uiGridConstants',
    'ProductService',
    function ($scope, uiGridConstants, ProductService) {

      $scope.populatePromise = [];
      /*Initial data population*/
      populate();

      /* Display configuration, initialization*/
      $scope.gridOptions = {
        data: 'products',
        enableSorting: true,
        enableFiltering: true,
        enableCellEdit: false,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        multiSelect: true,
        noUnselect: false,
        minRowsToShow: 10
      };

      $scope.gridOptions.columnDefs = [
        {
          name: 'productCode',
          minWidth: '150'
        },
        {
          name: 'description',
          minWidth: '150'
        },
        {
          name: 'finish',
          minWidth: '100'
        },
        {
          name: 'style',
          minWidth: '100'
        },
        {
          name: 'price',
          minWidth: '100',
          cellFilter: 'currency'
        }
      ];

      /*Local helper functions*/

      function populate() {
        $scope.populatePromise.push(
          ProductService.getAvailableProductsForQuotation($scope.quotationId).then(
            function successCallback(response) {
              $scope.products = response.data;
            }
          )
        );
      }

      /*Event handlers*/

      $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function () {
          $scope.selectedProducts.length = 0;
          angular.forEach(gridApi.selection.getSelectedRows(), function (product) {
            $scope.selectedProducts.push(product.id);
          });
        });
        gridApi.selection.on.rowSelectionChangedBatch($scope, function () {
          $scope.selectedProducts.length = 0;
          angular.forEach(gridApi.selection.getSelectedRows(), function (product) {
            $scope.selectedProducts.push(product.id);
          });
        });
      };

    }
  ]);
