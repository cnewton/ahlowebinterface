'use strict';

angular.module("ahlowebinterface")

  .controller("BuyerListController", [
    '$scope',
    'uiGridConstants',
    'BuyerService',
    '$window',
    '$state',
    function ($scope, uiGridConstants, BuyerService, $window, $state) {

      /*Initial Data Population*/
      populate();

      /* Display configuration, initialization*/
      $scope.gridOptions = {
        data: 'buyers',
        enableSorting: true,
        enableCellEdit: true,
        enableRowSelection: true,
        enableRowHeaderSelection: false,
        enableFiltering: true,
        multiSelect: false,
        noUnselect: true,
        minRowsToShow: 5
      };
      $scope.gridOptions.columnDefs = [
        {
          name: 'name',
          minWidth: '150'
        },
        {
          name: 'company',
          minWidth: '150'
        },
        {
          name: 'email',
          minWidth: '150'
        },
        {
          name: 'phone',
          minWidth: '110'
        },
        {
          name: 'address',
          minWidth: '200'
        }
      ];

      /*Local helper functions*/
      function populate() {
        BuyerService.all().then(
          function successCallback(response) {
            $scope.buyers = response.data;
          },
          function errorCallback() {
            $window.alert("Unable to contact server");
            $state.go('page.home');
          }
        );
      }

      /*Event handlers*/

      $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
          $scope.buyerSelected = row.isSelected;
          $scope.selectedBuyer = row.entity;
        });
        gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
          if (newValue !== oldValue) {
            BuyerService.update(rowEntity);
          }
        });
      };

      $scope.deleteBuyer = function () {
        BuyerService.delete($scope.selectedBuyer.id);
        var arrayPos = $scope.buyers.indexOf($scope.selectedBuyer);
        $scope.buyers.splice(arrayPos, 1);
        $scope.selectedBuyer = {};
        $scope.buyerSelected = false;
      };

      $scope.newBuyer = function () {
        var buyer = {
          name: "New Buyer",
          email: "e@mail.com",
          phone: "+85211111111",
          address: "",
          company: "",
          id: 0
        };
        BuyerService.create(buyer).success(function (data) {
          $scope.buyers.push(data);
        });
      };
    }
  ]);
