'use-strict';


angular.module('ahlowebinterface').run([
  '$rootScope',
  '$state',
  'AuthorisationService',
  'AuthenticationService',
  function ($rootScope, $state, AuthorisationService, AuthenticationService) {
        /**
         * Whenever the page changes, check to make sure the user is authorised for it.
         * If they are, go to it, otherwise make them login
         */
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            var authorised, currentUser;

            if (toState.access !== undefined && toState.access.loggedIn === true) {


                currentUser = AuthenticationService.getCurrentUser();

                if (currentUser === null || currentUser === undefined) {
                    event.preventDefault();
                    $state.go("login");
                } else {
                    authorised = AuthorisationService.authorise(currentUser, toState);

                    if (!authorised) {
                        event.preventDefault();
                        $state.go('page.home');
                    }
                }

            }
        });
}]);
