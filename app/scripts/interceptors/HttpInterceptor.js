'use-strict';

angular.module('ahlowebinterface')

.factory('httpRequestInterceptor', [
    'BACKEND',
    '$window',
    function (BACKEND, $window) {
        return {
            request: function (config) {
                config.headers[BACKEND.AUTH_HEADER] = $window.sessionStorage.getItem(BACKEND.AUTH_HEADER);
                return config;
            }
        };
    }
])

.config([
    '$httpProvider',
    function ($httpProvider) {
        $httpProvider.interceptors.push('httpRequestInterceptor');
}]);
